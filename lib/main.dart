import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Mac Journal',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Mac Journal Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  TextEditingController noteTextEditingController = TextEditingController();
  List<Note> notes = [];
  FocusNode keyboardListenerFocusnode = FocusNode();

  ScrollController notesScrollController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
                controller: notesScrollController,
                itemCount: notes.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    title: SelectableText(
                      notes[index].note,
                    ),
                    subtitle: SelectableText(
                        notes[index].dateTime.toLocal().toIso8601String()),
                  );
                }),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              decoration: BoxDecoration(
                border: Border.all(),
              ),
              child: RawKeyboardListener(
                focusNode: keyboardListenerFocusnode,
                onKey: (event) {
                  if (event.isKeyPressed(LogicalKeyboardKey.enter)) {
                    String note = noteTextEditingController.text.trim();
                    noteTextEditingController.text = "";

                    if (note.isNotEmpty) {
                      setState(() {
                        notes.add(Note(DateTime.now(), note));
                        notesScrollController.animateTo(
                          notesScrollController.position.maxScrollExtent + 70.0,
                          duration: const Duration(milliseconds: 300),
                          curve: Curves.fastOutSlowIn,
                        );
                      });
                    }
                  }
                },
                child: TextField(
                  controller: noteTextEditingController,
                  maxLines: 3,
                  maxLength: 1000,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class Note {
  final DateTime dateTime;
  final String note;
  Note(this.dateTime, this.note);
}
